#! /usr/bin/fish

set algs is1 is2 bf ms1 ms2 ms3
set ord inc dec rnd

# small tests

for alg in $algs
    for o in $ord
        for i in (seq 1000 1000 50000)
            set sum 0
            set reps 100
            set cnt 0
            for r in (seq 1 $reps)
                 set a (./a.out -a $alg -s $o -n $i)
                echo $a[1]
                set sum (math $sum + $a[1])
                set cnt (math $cnt + 1)
                if math "$sum>1000" > /dev/null
                    break
                end
            end
            set sum (math $sum / $cnt)
            echo $i,$sum | tee -a tests/"small-"$alg"-"$o".out"
        end
    end
end

#large tests


for alg in $algs
    set i 10000
    set reps
    while true
        set i (math -s0 $i /0.8)
        if math "$i > 1000000000" > /dev/null
            break
        end
        set sum 0
        set reps 100
        set cnt 0
        for r in (seq 1 $reps)
            set a (./a.out -a $alg -s rnd -n $i)
            echo $a[1]
            set sum (math $sum + $a[1])
            set cnt (math $cnt + 1)
            if math "$sum>1000" > /dev/null
                break
            end
        end
        set sum (math $sum / $cnt)
        echo $i,$sum | tee -a tests/"large-"$alg".out"
        if math "$sum>10000" > /dev/null
            break
        end
    end
end

#tiny tests

#set algs is1 is2 bf ms1 ms2 ms3 hybrid

# set algs hybrid


# for alg in $algs
#     set reps 100000
#     for i in (seq 200 50 2000)
#         set a (./a.out -a $alg -s rnd -n $i -r $reps)
#         echo $i,$reps,$a[1] | tee -a tests/"less-tiny-"$alg".out"
#         if math "$a[1]>2000" > /dev/null 
#             set reps (math $reps / 2)
#         end
#     end
# end

