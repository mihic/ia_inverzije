
#include <stdio.h>
#include <cstdlib> 
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <vector>
#include <random>
#include <algorithm>
#include <chrono>

using namespace std;

enum Algorithm {bruteforce,insertion1,insertion2,mergesort1,mergesort2,mergesort3,hyb};
enum Generator {incr,decr,rnd};

void vector_print(vector<int> sez){
    printf("[");
    for (int a : sez){
        printf("%d",a);
    }
    printf("]\n");
}

int bf(vector<int> &sez){
    int n = sez.size();
    int cnt = 0;
    for (int i=0;i<n;i++){
        for (int j=i+1;j<n;j++){
            if (sez[j]<sez[i]) cnt++;
        }
    }
    return cnt;
}
int ss2(vector<int> sez){
    int n = sez.size();
    int cnt = 0;
    for (int i = 0;i<n;i++){
        int idx = i;
        int min = sez[i];
        for (int j=i+1;j<n;j++){
            if (sez[j]<min){
                min = sez[j];
                idx = j;
            }
        }
        int tmp = sez[idx];
        cnt+=idx-i;
        //seznam zamaknemo za ena desno (od i do idx)
        for (int j=idx;j>i;j--){
            sez[j]=sez[j-1];
        }
        sez[i]=tmp;
        
    }
    return cnt;
}

int is1(vector<int> sez){
    int n = sez.size();
    int cnt = 0;
    for(int i=1; i<n; ++i) {
        int j;
        for(j=i; j>0 && sez[j]<sez[j-1]; --j) {
            swap(sez[j-1],sez[j]);
        }
        cnt += i-j;
    }
    return cnt;
}
int is2(vector<int> sez){
    int n = sez.size();
    int cnt = 0;
    for(int i=1; i<n; i++) {
        int tmp = sez[i];
        int j;
        for(j=i; j>0 && tmp<sez[j-1]; --j) {
            sez[j] = sez[j-1];
        }
        sez[j] = tmp;
        cnt += i-j;
    }
    return cnt;
}


int ss1(vector<int> sez){
    int n = sez.size();
    int cnt = 0;
    for (int i = 0;i<n;i++){
        //vector_print(sez);
        int idx = i;
        int min = sez[i];
        for (int j=i+1;j<n;j++){
            if (sez[j]<min){
                min = sez[j];
                idx = j;
            }
        }
        int tmp;
        cnt+=idx-i;
        //seznam zamaknemo za ena desno (od i do idx)
        for (int j=idx;j>i;j--){
            tmp = sez[j];
            sez[j] = sez[j-1];
            sez[j-1]=tmp;
        }
        
    }
    return cnt;
}



int ms1_rec(vector<int> &sez){
    int n = sez.size();
    if (n==1) return 0;
    int cnt=0;
    vector<int> l;
    vector<int> d;

    for (int i = 0; i<n;i++){
        if (i<n/2){
            l.push_back(sez[i]);
        } else {
            d.push_back(sez[i]);
        }
    }

    cnt+=ms1_rec(l);
    cnt+=ms1_rec(d);

    sez.clear();
    int i=0;
    int j=0;
    int lenL = l.size();
    int lenD = d.size();
    while (i<lenL && j<lenD){
        if (l[i]<=d[j]) sez.push_back(l[i++]);
        else {
            sez.push_back(d[j++]);
            cnt+=lenL-i;
        }
    }
    while(i<lenL){
        sez.push_back(l[i++]);
    }
    while(j<lenD){
        sez.push_back(d[j++]);
    }

    // printf("after merge");
    //vector_print(sez);
    return cnt;    
}
int ms1(vector<int> sez){
    ms1_rec(sez);
}

int ms2_rec(vector<int> &s1, vector<int> &s2,int l, int d){
    if (l==d){
        s2[l]=s1[l];
        return 0;
    }
    int cnt = 0;
    int m = (l+d)/2;
    cnt+=ms2_rec(s2,s1,l,m);
    cnt+=ms2_rec(s2,s1,m+1,d);
    int i = l;
    int j = m+1;
    int t = l;
    while (i<=m && j<=d){
        if (s1[i]<=s1[j]) s2[t++]=s1[i++];
        else {
            s2[t++]=s1[j++];
            cnt+=m-i+1;
        }
    }
    while (i<=m){
        s2[t++]=s1[i++];
    }
    while (j<=d){
        s2[t++]=s1[j++];
    }
    return cnt;
}


int ms3(vector<int> s1){
    int n = s1.size();
    vector<int> s2(n);
	int cnt = 0;
    for (int level=1; level<n; level *=2){
		for (int first=0; first<n; first+=2*level){
			int mid = min(first+level,n);
			int last = min(mid+level,n);
			int k0=first;
			int k1=mid;
			for (int k=first; k<last; ++k){
				if (k1 != last && (k0==mid || s1[k1] < s1[k0])){
					s2[k] = s1[k1++];
					cnt += mid-k0;
				}else {
					s2[k] = s1[k0++];
				}
			}
		}	
		move(begin(s2),end(s2),begin(s1));
	}
    return cnt;
}

int hybrid(vector<int> sez){
    if (sez.size()>1100){
        return ms3(sez);
    }else 
        return bf(sez);
}

int ms2(vector<int> sez){
    int n = sez.size();
    vector<int> s2(n);
    for (int i=0;i<n;i++){
        s2[i]=sez[i];
    }
    return ms2_rec(sez,s2,0,n-1);
}
 
//template <typename F>
void test(auto tests, auto f){
//void test(vector<vector<int>> tests, int(*f)(vector<int>)){
    int r = tests.size();

	std::chrono::time_point<std::chrono::steady_clock> _TEST_start, _TEST_end; 
	_TEST_start = std::chrono::steady_clock::now();
    int result = 0;
    for (int i=0;i<r;++i){
	   result += f(tests[i]);
	}

    _TEST_end = std::chrono::steady_clock::now();                              
	auto _TEST_elapsed = std::chrono::duration_cast<std::chrono::milliseconds> (_TEST_end - _TEST_start);  
    printf("%d\n%d\n",_TEST_elapsed.count(),result);
}


int main(int argc, char* argv[]){
    int N = 100;
    int R = 1;
    int c;

    Algorithm alg = bruteforce;
    Generator gen = decr;

	random_device rd;
	auto seed = rd();
	default_random_engine random{seed};
	
	//parsing parameters
    while ((c = getopt(argc,argv,"a:n:s:r:"))  != -1){
        switch(c){
            case 'a':
                if (strcmp("bf",optarg)==0) alg=bruteforce;
                else if (strcmp("is1",optarg)==0) alg=insertion1;
                else if (strcmp("is2",optarg)==0) alg=insertion2;
                else if (strcmp("ms1",optarg)==0) alg=mergesort1;
                else if (strcmp("ms2",optarg)==0) alg=mergesort2;
                else if (strcmp("ms3",optarg)==0) alg=mergesort3;
                else if (strcmp("hybrid",optarg)==0) alg=hyb;
                else {
                    printf("unknown algorrithm %s",optarg);
                    return -1;
                }
                break;
            
            case 's':
                if (strcmp("inc",optarg)==0) gen=incr;
                else if (strcmp("dec",optarg)==0) gen=decr;
                else if (strcmp("rnd",optarg)==0) gen=rnd;
                else {
                    printf("unknown sequnce description %s",optarg);
                    return -1;
                }
                break;
            case 'n':
                N = atoi(optarg);
                break;
            case 'r':
                R = atoi(optarg);
                break;
            case '?':
                if (optopt == 'a'  || optopt == 'n' || optopt == 'r' || optopt == 's')
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr, "Unknown option character `\\x%x'.\n",optopt);
                break;
        }
    }
  //genereate test list

    vector<vector<int>> tests;
    for(int i=0;i<R;++i){
        vector<int> list;
        switch (gen){
        case incr:
            for (int i = 0; i<N;i++){
                list.push_back(i);
            }
            break;
        case decr:
            for(int i = N; i>0; i--){
                list.push_back(i);
            }
            break;
        case rnd:
            for (int i = 0; i<N;i++){
                list.push_back(i);
            }
            shuffle(list.begin(),list.end(),random);
            break;
        }
        tests.push_back(list);
    }


	//test the algorithm
    switch(alg){
        case bruteforce:
            //printf("doing bruteforce\n");
            //printf("n=%d\n",N);
            test(tests,&bf);
			break;
        case insertion1:
            // printf("doing insertion1\n");
            test(tests,&is1);
            break;
        case insertion2:
            // printf("doing insertion2\n");
            test(tests,&is2);
            break;
        case mergesort1:
            // printf("doing merge sort 1\n");
            test(tests,&ms1);
            break;
        case mergesort2:
            // printf("doing merge sort 2\n");
            test(tests,&ms2);
            break;
        case mergesort3:
            // printf("doing merge sort 3\n");
            test(tests,&ms3);
            break;
        case hyb:
            // printf("doing merge sort 3\n");
            test(tests,&hybrid);
            break;
        default:
            printf("not implemented\n");
            return -1;
    }
}
